import { Component, OnInit } from '@angular/core';
import {CarService} from '../car.service';
import {Car} from '../car';

@Component({
  selector: 'app-onsale',
  templateUrl: './onsale.component.html',
  styleUrls: ['./onsale.component.css']
})
export class OnsaleComponent implements OnInit {
  cars: Car[] = [];
  constructor(private carService: CarService) { }

  ngOnInit() {
    this.getCars();
  }

  getCars(): void {
    this.carService.getCars().subscribe(cars => this.cars = cars.filter(v => (v.ordered === false || v.ordered === undefined)));
  }

}
