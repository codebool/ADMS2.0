export interface Car {
  id: number;
  name: string;
  year: number;
  brand: string;
  timeIn: string;
  ordered: boolean;
  msrp: number;
  discount: number;
  color: string;
  trim: string;
  cId: number;
}

export interface Customer {
  cId: number;
  cName: string;
  cGender: string;
  cPhone: string;
  cEmail: string;
}
