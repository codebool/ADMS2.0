import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {Car} from './car';

const httpOptions = {
  headers: new HttpHeaders({'content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class CarService {
  private carUrl = `http://localhost:3000/cars`;

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

  constructor(private http: HttpClient) { }

  getCars(): Observable<Car[]> {
    return this.http.get<Car[]>(this.carUrl).pipe(
      catchError(this.handleError(`getcars`, []))
    );
  }

  getCarNo404<Data>(id: number): Observable<Car> {
    const url = `${this.carUrl}/?id=${id}`;
    return this.http.get<Car[]>(url).pipe(
      map(cars => cars[0]));
  }

  getCar(id: number): Observable<Car> {
    const url = `${this.carUrl}/${id}`;
    return this.http.get<Car>(url).pipe(
      catchError(this.handleError<Car>(`getcar id=${id}`))
    );
  }

  addCar(car: Car): Observable<Car> {
    return this.http.post<Car>(this.carUrl, car, httpOptions).pipe(
      catchError(this.handleError<Car>('addcar'))
    );
  }

  updateCar(car: Car): Observable<any> {
    return this.http.put(this.carUrl + '/' + car.id, car, httpOptions).pipe(
      catchError(this.handleError<any>('updatecar'))
    );
  }

  deleteCar(car: Car | number): Observable<Car> {
    const id = typeof car === 'number' ? car : car.id;
    const url = `${this.carUrl}/${id}`;

    return this.http.delete<Car>(url, httpOptions).pipe(
      catchError(this.handleError<Car>('deletecar'))
    );
  }
}
