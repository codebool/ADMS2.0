import {Car, Customer} from './car';

export const CUSTOMERS: Customer[] = [
  {
    'cId': 1,
    'cName': 'Ben',
    'cGender': 'Male',
    'cPhone': '613-898-6700',
    'cEmail': 'ben@gmail.com'
  },
  {
    'cId': 2,
    'cName': 'Ann',
    'cGender': 'Female',
    'cPhone': '519-123-7890',
    'cEmail': 'ann@apple.com'
  },
  {
    'cId': 3,
    'cName': 'Mark',
    'cGender': 'Male',
    'cPhone': '519-339-0012',
    'cEmail': 'mark@facebook.com'
  },
  {
    'cId': 4,
    'cName': 'Luke',
    'cGender': 'Male',
    'cPhone': '519-668-7731',
    'cEmail': 'luke@amazon.com'
  },
  {
    'cId': 5,
    'cName': 'Jack',
    'cGender': 'Male',
    'cPhone': '289-222-9996',
    'cEmail': 'jack@linkedin.com'
  },
  {
    'cId': 6,
    'cName': 'Christina',
    'cGender': 'Female',
    'cPhone': '519-355-4430',
    'cEmail': 'chris@walmart.com'
  },
  {
    'cId': 7,
    'cName': 'Julia',
    'cGender': 'Female',
    'cPhone': '613-221-0099',
    'cEmail': 'julia@nokia.com'
  },
  {
    'cId': 8,
    'cName': 'Bill',
    'cGender': 'Male',
    'cPhone': '800-000-0000',
    'cEmail': 'bill@hotmail.com'
  }
];

export const CARS: Car[] = [
  {
    'id': 1,
    'name': 'X3',
    'year': 2015,
    'brand': 'BMW',
    'timeIn': '2018-07-01',
    'ordered': false,
    'msrp': 33000,
    'discount': 29899,
    'color': 'Black',
    'trim': 'xDriver28i',
    'cId': 1
  },
  {
    'id': 2,
    'name': 'G550',
    'year': 2017,
    'brand': 'Benz',
    'timeIn': '2018-04-01',
    'ordered': false,
    'msrp': 55000,
    'discount': 45990,
    'color': 'Yellow',
    'trim': 'BRABUS',
    'cId': 2
  },
  {
    'id': 3,
    'name': 'RAV4',
    'year': 2016,
    'brand': 'TOYOTA',
    'timeIn': '2018-10-01',
    'ordered': true,
    'msrp': 22000,
    'discount': 18995,
    'color': 'Light Grey',
    'trim': 'AWD',
    'cId': 3
  },
  {
    'id': 4,
    'name': 'Pilot',
    'year': 2017,
    'brand': 'HONDA',
    'timeIn': '2018-06-01',
    'ordered': false,
    'msrp': 37000,
    'discount': 33995,
    'color': 'Silver',
    'trim': '4WD 4dr LX 8 Passenger',
    'cId': 4
  },
  {
    'id': 5,
    'name': 'Santa',
    'year': 2011,
    'brand': 'HYUNDAI',
    'timeIn': '2017-12-01',
    'ordered': true,
    'msrp': 12999,
    'discount': 10595,
    'color': 'Dark Grey',
    'trim': 'AWD',
    'cId': 5
  },
  {
    'id': 6,
    'name': 'MDX',
    'year': 2017,
    'brand': 'ACURA',
    'timeIn': '2018-08-01',
    'ordered': true,
    'msrp': 49599,
    'discount': 45995,
    'color': 'Black',
    'trim': 'Navigation Package',
    'cId': 6
  },
  {
    'id': 7,
    'name': 'A4',
    'year': 2017,
    'brand': 'AUDI',
    'timeIn': '2018-09-01',
    'ordered': true,
    'msrp': 42000,
    'discount': 39722,
    'color': 'Blue',
    'trim': 'S-Line Premium Plus',
    'cId': 7
  },
  {
    'id': 8,
    'name': 'Edge',
    'year': 2014,
    'brand': 'FORD',
    'timeIn': '2017-12-12',
    'ordered': false,
    'msrp': 26000,
    'discount': 24395,
    'color': 'White',
    'trim': 'Sport',
    'cId': 8
  }
];
