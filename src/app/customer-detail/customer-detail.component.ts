import {Component, Input, OnInit} from '@angular/core';
import {CUSTOMERS} from '../mock-cars';

@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.css']
})
export class CustomerDetailComponent implements OnInit {
  customers = CUSTOMERS;
  @Input() cId: number;
  constructor() { }

  ngOnInit() {
  }

}
