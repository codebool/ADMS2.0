import { Component, OnInit } from '@angular/core';
import {Car} from '../car';
import {CarService} from '../car.service';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {
  cars: Car[];
  constructor(private carService: CarService) { }

  ngOnInit() {
    this.getCars();
 }

  getCars(): void {
    this.carService.getCars().subscribe(cars => this.cars = cars);
  }

  add(brand: string, name: string): void {
    name = name.trim();
    brand = brand.trim();
    if (name) {
      if (brand) {
        this.carService.addCar({name, brand} as Car).subscribe(car => this.cars.push(car));
      } else {
        return;
      }
    } else {
      return;
    }
  }

  delete(car: Car): void {
    this.cars = this.cars.filter(c => c !== car);
    this.carService.deleteCar(car).subscribe();
  }
}
