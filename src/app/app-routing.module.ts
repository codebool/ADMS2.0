import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CarsComponent} from './cars/cars.component';
import {OnsaleComponent} from './onsale/onsale.component';
import {CarDetailComponent} from './car-detail/car-detail.component';

const routes: Routes = [
  { path: '', redirectTo: '/onsale', pathMatch: 'full' },
  { path: 'cars', component: CarsComponent },
  { path: 'onsale', component: OnsaleComponent },
  { path: 'detail/:id', component: CarDetailComponent }
];

@NgModule({
  exports: [ RouterModule ],

  imports: [ RouterModule.forRoot(routes) ]
})
export class AppRoutingModule { }
